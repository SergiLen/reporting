## Requirments
### To install all the requirements run `pip install -e .` from the current directory

## Running
### To run the application 
* change directory into report_service and run `python report_service/app.py` 
* navigate to http://127.0.0.1:5000/ 
api endpoints 
* /reports/report_id the HTML of the report 
* /reports/report_id/xml the xml of the report
* /reports/report_id/pdf the pdf of the report



#### The application is tested to work in Ubuntu 16.04.


