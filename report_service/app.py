from flask import Flask, render_template, url_for,make_response
from flask_sqlalchemy import SQLAlchemy
import json, dicttoxml
from flask_weasyprint import  render_pdf, HTML


app = Flask(__name__)
# Setup the db connection 
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://interview:uo4uu3AeF3@candidate.suade.org/suade'
db = SQLAlchemy(app)

# Report class for the reports table
class Report(db.Model):
    __tablename__ = "reports"
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String())

# get the data from database for the requested id
# if the report_id not in the database will return an empty report
def get_data(report_id):
    reported=db.session.query(Report).filter(Report.id == report_id).one_or_none()
    if reported:
        reported_data=reported.data
        reported_dictionary=json.loads(reported_data)
    else:
        reported_dictionary={}
    return reported_dictionary


# Html page with the report id
@app.route('/reports/<report_id>', methods=["GET"])
def report_html(report_id):
    reported_dictionary=get_data(report_id)
    return render_template('report.html' , reported_dictionary=reported_dictionary)

# xml endpoint
@app.route('/reports/<report_id>/xml', methods=["GET"])
def report_xml(report_id):
    reported_dictionary=get_data(report_id)
    xmls = dicttoxml.dicttoxml(reported_dictionary, custom_root='report')
    response= make_response(xmls)
    response.headers["Content-Type"] = "application/xml"   
    return response

# pdf endpoint
@app.route('/reports/<report_id>/pdf', methods=["GET"])
def report_pdf(report_id):
    reported_dictionary= get_data(report_id)
    html= render_template('report.html' , reported_dictionary=reported_dictionary)
    return render_pdf(HTML(string=html))
        


if __name__== '__main__':
    app.debug = True
    app.run()